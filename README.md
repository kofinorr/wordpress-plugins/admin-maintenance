# Admin Maintenance
You have to do some modifications to your website, but you don't want to stop your customers of using your website ? This plugins is want you need ! With a single click, put the administrations pages into maintenance mode, without disabling your website for your customers. And if for one reason or another you need to do some modifications, you will be able to deactivate the splash screen just for you the time you do your modifications.

## Requirements
* Require WordPress 4.7+ / Tested up to 5.6
* Require PHP 5.6

## Installation

### Manual
- Download and install the plugin using the built-in WordPress plugin installer.
- No settings necessary, it's a Plug-and-Play plugin !

### Composer
- Add the following repository source : 
```
    {
        "type": "vcs",
        "url": "https://gitlab.com/kofinorr/wordpress-plugins/admin-maintenance.git"
    }
```
- Include `"kofinorr/admin-maintenance": "dev-master"` in your composer file for last master's commits or a tag released.
- No settings necessary, it's a Plug-and-Play plugin !

## Internationalisation
* English *(default)*
* Français

## License
"Admin Maintenance" is licensed under the GPLv3 or later.
