<?php
/**
 * Plugin Name: Admin Maintenance
 * Description: Put the administration pages into maintenance mode, without disabling your website for your customers.
 * Version: 1.0.0
 * Author: Kofinorr
 * Author URI: https://kofinorr.damien-roche.fr/
 * Text Domain: krr-admin-maintenance
 * Domain Path: /languages
 * License: GPLv3 or later
 */

namespace KrrAdminMaintenance;

/**
 * Class AdminMaintenance
 * @package KrrAdminMaintenance
 */
class AdminMaintenance
{
	/* URL Parameter */
	const PARAMETER = 'krr_admin_maintenance';

	/* Name of the database option */
	const OPTION_NAME = 'krr_admin_maintenance';

	/**
	 * AdminMaintenance constructor.
	 */
	public function __construct()
	{
		add_action('init', [$this, 'load_i18n']);
		add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 11);
		add_filter('admin_body_class', [$this, 'bodyClass']);
		add_action('admin_bar_menu', [$this, 'toolbarLink'], 50);
		add_action('admin_head', [$this, 'updateOptions']);
		add_action('admin_footer', [$this, 'screenRenderer']);
		add_action('all_admin_notices', [$this, 'unlockedNotice'], 5);
	}

	/**
	 * Load plugin textdomain.
	 */
	public function load_i18n() {
		load_plugin_textdomain(
			'krr-admin-maintenance',
			false,
			dirname(plugin_basename(__FILE__)) . '/languages'
		);
	}

	/**
	 *  Styles and scripts
	 */
	public function enqueueScripts()
	{
		wp_enqueue_style(
			'krr-admin-maintenace',
			plugin_dir_url(__FILE__) . 'assets/style.css',
			[],
			false
		);
	}

	/**
	 *  Add the loading class to the body
	 */
	public function bodyClass($classes)
	{
		$user = wp_get_current_user();
		if(get_user_meta($user->ID, self::OPTION_NAME, true) === 'forced') {
			return $classes . ' adminMaintenanceAccessForced';
		}
	}

	/**
	 * Add the link to pass into maintenance mode
	 * @param $wp_admin_bar
	 */
	public function toolbarLink($wp_admin_bar)
	{
		/** @var wp_admin_bar $wp_admin_bar */
		global $wp_admin_bar;
		$user = wp_get_current_user();

		/* Enable the link for administrator only */
		/* TODO: Make this editable */
		if (!user_can($user->ID, 'administrator')) {
			return;
		}

		/* Get the maintenance mode state */
		$maintenanceModeState = get_option(self::OPTION_NAME);

		/* Dynamic elements */
		$title   = ($maintenanceModeState === 'disable') ? __('Enable maintenance mode', 'krr-admin-maintenance') : __('Disable maintenance mode', 'krr-admin-maintenance');
		$toggle  = ($maintenanceModeState === 'disable') ? 'enable' : 'disable';
		$onclick = ($maintenanceModeState === 'enable') ? 'return confirm("' .  __('Are you sure you want to disable maintenance mode ?', 'krr-admin-maintenance') . '")' : '';

		/* Create the link node */
		$wp_admin_bar->add_node([
			'id'     => 'admin-blocker',
			'parent' => 'site-name',
			'title'  => $title,
			'href'   => admin_url() . '?' . self::PARAMETER . '=' . $toggle,
			'meta'   => [
				'onclick' => $onclick
			]
		]);
	}

	/**
	 * Update the option depending on the url parameter
	 */
	public function updateOptions()
	{
		if (!isset($_GET[self::PARAMETER])) {
			return;
		}

		/* Enable/Disable the maintenance mode */
		if ($_GET[self::PARAMETER] === 'enable' || $_GET[self::PARAMETER] === 'disable') {
			update_option(self::OPTION_NAME, $_GET[self::PARAMETER]);

			/* Remove all users forced options */
			if ($_GET[self::PARAMETER] === 'disable') {
				$users = get_users([
					'meta_key'   => self::OPTION_NAME,
					'meta_value' => 'forced',
				]);

				foreach	($users as $user) {
					update_user_meta($user->ID, self::OPTION_NAME, '');
				}
			}
		}

		/* Force admin access for current user */
		elseif ($_GET[self::PARAMETER] === 'user_forced') {
			$user = wp_get_current_user();
			update_user_meta($user->ID, self::OPTION_NAME, 'forced');
		}

		/* Unforced admin access for current user */
		elseif ($_GET[self::PARAMETER] === 'user_unforced') {
			$user = wp_get_current_user();
			update_user_meta($user->ID, self::OPTION_NAME, '');
		}
	}

	/**
	 * Screen of maintenance mode
	 */
	public function screenRenderer()
	{
		$user = wp_get_current_user();

		/* Get the maintenance mode state */
		$maintenanceModeState = get_option(self::OPTION_NAME);
		$screenClass = ($maintenanceModeState === 'enable') ? ' active' : '';

		/* Define the messages */
		$mainMessage         = __('Maintenance mode', 'krr-admin-maintenance');
		$alertMessage        = __('Attention !!! The administration is in maintenance mode, you have forced the access to admin panel !', 'krr-admin-maintenance');
		$forcedMessage       = __('Force access to administration', 'krr-admin-maintenance');
		$unforcedMessage     = __('Get back to maintenance mode', 'krr-admin-maintenance');
		$disableMessage      = __('Disable maintenance mode', 'krr-admin-maintenance');
		$confirmationMessage = __('Are you sure you want to disable maintenance mode ?', 'krr-admin-maintenance');

		/* Define the urls */
		$disableUrl   = admin_url() . '?' . self::PARAMETER . '=disable';
		$forcedUrl    = admin_url() . '?' . self::PARAMETER . '=user_forced';
		$unforcedUrl  = admin_url() . '?' . self::PARAMETER . '=user_unforced';

		$buttons = '';

		// Ajoute les boutons pour les administrateurs
		if (user_can($user->ID, 'administrator')) {

			/* Buttons structure */
			$buttons = <<<HTML
<div class="adminMaintenance-buttons">
    <a class="adminMaintenance-forced button button-secondary" href="{$forcedUrl}" >
        {$forcedMessage}
    </a>
    <a class="adminMaintenance-unlock button button-primary" href="{$disableUrl}" onclick='return confirm("{$confirmationMessage}")'>
        {$disableMessage}
    </a>
</div>
HTML;
		}

		/* Global structure of the maintenance screen */
		$mainScreen = <<<HTML
<div class="adminMaintenance {$screenClass}">
    <div class="adminMaintenance-label">{$mainMessage}</div>
    {$buttons}
</div>
HTML;

		/* Structure of the notice maintenance screen when a user forced access */
		$messageScreen = <<<HTML
<div class="adminMaintenanceMessage">
    <span>{$alertMessage}</span>
     <a class="adminMaintenance-unforced button button-link-delete" href="{$unforcedUrl}" >
        {$unforcedMessage}
    </a>
</div>
HTML;

		/* When a user has forced the access, we show the alert message */
		if(get_user_meta($user->ID, self::OPTION_NAME, true) === 'forced') {
			echo $messageScreen;
		}

		/* If not, and if the maintenance mode is enable, we show the splash screen */
		else {
			if($maintenanceModeState === 'enable') {
				echo $mainScreen;
			}
		}
	}

	/**
	 * Message to notice that the maintenance mode has been disabled
	 */
	public function unlockedNotice()
	{
		/* Security */
		if (!isset($_GET[self::PARAMETER]) || $_GET[self::PARAMETER] == 'enable') {
			return;
		}

		echo '<div class="updated"><p><strong>' . __('Maintenance mode has been disabled.', 'krr-admin-maintenance') . '</strong></p></div>';
	}
}

new AdminMaintenance();