��          �       �       �      �   3   �   d   3     �     �     �     �            #     f   C     �  �  �     Y  <   k  g   �          0  #   L     p     �     �  (   �  _   �     /   Admin Maintenance Are you sure you want to disable maintenance mode ? Attention !!! The administration is in maintenance mode, you have forced the access to admin panel ! Disable maintenance mode Enable maintenance mode Force access to administration Get back to maintenance mode Kalimorr Maintenance mode Maintenance mode has been disabled. Put the administration pages into maintenance mode, without disabling your website for your customers. https://github.com/Kalimorr Project-Id-Version: Admin Maintenance
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-01 17:15+0000
PO-Revision-Date: 2021-01-16 12:23+0000
Last-Translator: 
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.3; wp-5.4.4 Admin Maintenance Êtes-vous sûr de vouloir désactiver le mode maintenance ? Attention !!! L'administration est en mode maintenance, vous avez forcé l'accès à l'administration ! Désactiver le mode maintenance Activer le mode maintenance Forcer l'accès à l'administration Revenir en mode maintenance Kalimorr Mode maintenance Le mode maintenance a été désactivé. Place l'administration en mode maintenance, sans impacter votre site web pour vos utilisateurs. https://github.com/Kalimorr 